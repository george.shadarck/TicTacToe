public enum GameChars {
    error{
        @Override
        public String toString() {
            return "error";
        }
    },
    blank{
        @Override
        public String toString(){
            return "blank";
        }
    },
    cross{
        @Override
        public String toString(){
            return "cross";
        }
    },
    zero{
        @Override
        public String toString(){
            return "zero";
        }
    }
}