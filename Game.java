

public class Game {
    private GameChars Table[][] = {{GameChars.blank, GameChars.blank, GameChars.blank},
                                    {GameChars.blank, GameChars.blank, GameChars.blank},
                                    {GameChars.blank, GameChars.blank, GameChars.blank}};


    public GameChars[][] getTable(){
        return Table.clone();
    }

    public GameChars getCell(byte cell){
        if(0 <= cell && cell <= 8) return Table[(int)cell / 3][cell % 3];
            return GameChars.error;
    }

    public void setCell(byte cell, GameChars value){
        if(0 <= cell && cell <= 8)
            Table[(int)cell / 3][cell % 3] = value;
    }

    public GameChars getWiner(){
        for(GameChars[] it: Table)
            if(it[0].equals(it[1]) && it[1].equals(it[2]) && !it[0].equals(GameChars.blank))
                return it[0];
        for(int i = 0; i < 3; i++)
            if(Table[0][i].equals(Table[1][i]) && Table[1][i].equals(Table[2][i]) && !Table[0][i].equals(GameChars.blank))
                return Table[0][i];
        if(Table[0][0].equals(Table[1][1]) && Table[1][1].equals(Table[2][2]) && !Table[0][0].equals(GameChars.blank))
            return Table[0][0];
        if(Table[2][0].equals(Table[1][1]) && Table[1][1].equals(Table[0][2]) && !Table[1][1].equals(GameChars.blank))
            return Table[1][1];
        else return GameChars.blank;
    }

    public boolean isRoundDrawn(){
        for(GameChars[] it: Table)
            for(GameChars i: it)
                if(i.equals(GameChars.blank)) return false;
        return getWiner().equals(GameChars.blank);
    }

    @Override
    public String toString(){
        String result = "---------";
        for(GameChars[] it: Table){
            result += '\n';
            for(GameChars i: it){
                switch(i) {
                    case error:
                        break;
                    case blank:
                        result += "| |";
                        break;
                    case cross:
                        result += "|x|";
                        break;
                    case zero:
                        result += "|0|";
                        break;
                    default:
                        break;
                }
            }
            result += "\n---------";
        }
        return result;
    }

}